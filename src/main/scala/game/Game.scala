package game

import cell_status.CellStatus.{Empty, O, X}
import field.Field
import players.Player
import state.GameState

import scala.annotation.implicitNotFound


object Game {

  def main(args: Array[String]): Unit = {
    playGame()
  }

  def playGame(): Unit = {

    val field = Field(Vector.fill(10, 5)(Empty), 10, 5)
    implicit val currentPlayer: Player = Player(X)
    implicit val gameState: GameState = GameState(field, currentPlayer)

    playGameHelper
  }

  @implicitNotFound("No state is found in function call context!")
  def playGameHelper(implicit gameState: GameState): GameState = {
    import scala.io.Source.stdin
    gameState.field.printSelf()
    stdin.getLines()
      .foldLeft(GameState(gameState.field, gameState.currentPlayer))((currentState, userInput) => {
        val chosenColumn = userInput.toInt
        val stateAfterMove =
          if (chosenColumn > currentState.field.board.length) currentState.copy(output = "Wrong column number!")
          else performMove(currentState, chosenColumn)

        if (stateAfterMove.output != "") println(stateAfterMove.output)
        stateAfterMove.field.printSelf()
        stateAfterMove
      })
  }

  @implicitNotFound(s"No current player found in function call context!")
  def switchPlayers(implicit currPlayer: Player): Player = currPlayer.diskType match {
    case X => Player(O)
    case O => Player(X)
  }

  val performMove: (GameState, Int) => GameState = (currentState: GameState, columnNumber: Int) => {
    val board = currentState.field.board
    val selectedColumn = board(columnNumber)
    val indexOfEmpty = selectedColumn.lastIndexOf(Empty)
    indexOfEmpty match {
      case -1 => currentState.copy(output = "Column is full. Choose another!")
      case _ =>
        implicit val newCurrentPlayer: Player = currentState.currentPlayer
        val updatedColumn = selectedColumn.updated(indexOfEmpty, currentState.currentPlayer.diskType)
        val newBoard = board.updated(columnNumber, updatedColumn)
        GameState(Field(newBoard, 10, 5), switchPlayers)
    }
  }
}
