package field

import cell_status.CellStatus.CellStatus

case class Field(board: Vector[Vector[CellStatus]], sizeX: Int, sizeY: Int) {
  def printSelf(): Unit = {
    print("┌")
    1.until(sizeX) foreach { _ =>
      print("─┬")
    }
    print("─┐")
    println()
    0.until(sizeY) foreach { i =>
      print("│")
      board foreach {
        column => {
          print(column(i) + "│")
        }
      }
      print("\n")
    }
    print("└")
    1.until(sizeX) foreach { _ =>
      print("─┴")
    }
    print("─┘")
    println()
    print(" ")
    0.until(sizeX) foreach { i =>
      print(i + " ")
    }
    println()
  }
}
