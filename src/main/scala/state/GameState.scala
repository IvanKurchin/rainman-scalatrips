package state

import field.Field
import players.Player

case class GameState(field: Field, currentPlayer: Player, output: String = "")
