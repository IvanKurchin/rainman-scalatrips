package cell_status

object CellStatus extends Enumeration {
  type CellStatus = Value

  val X: CellStatus.Value = Value("●")
  val O: CellStatus.Value = Value("○")
  val Empty: CellStatus.Value = Value(" ")
}
