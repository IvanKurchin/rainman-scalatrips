package game

import cell_status.CellStatus.{O, X}
import org.scalatest.FunSuite
import players.Player

class GameTest extends FunSuite {

  test("it tests that players are switching from Player(X) to Player(O)") {
    val currentPlayer = Player(X)
    val switchedPlayer = Game.switchPlayers(currentPlayer)
    assert(switchedPlayer.equals(Player(O)))
  }

  test("It tests that players are switching from Player(O) to Player(X)") {
    val currentPlayer = Player(O)
    val switchedPlayer = Game.switchPlayers(currentPlayer)
    assert(switchedPlayer.equals(Player(X)))
  }
}
